# BrutBox

The BrutBox is an open source midi device based on a Teensy microcontroller. The software is based on Pure Data and our [Malinette](http://malinette.info) project. You should follow this link to install it.

Website: http://reso-nance.org/brutbox
Documentation: http://reso-nance.org/wiki/projets/brutbox/accueil


